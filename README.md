# TRIPTYCH #



### What is TRIPTYCH? ###

**TRIPTYCH** (**Tr**avel **I**n-**P**erson **T**asks for **Y**our **C**onsideration & **H**ome Education) is a collection of three post-COVID **OpenShift** apps: 
**WanderList**, **Vax Populi**, and **LessonPlan**.

**WanderList** is a travel itinerary app.

**Vax Populi** is a **Verifiable Credential** (**VC**) passport that verifies the fully vaccinated status of a holder against **COVID-19**.   

**LessonPlan** is an enrollment, syllabus, and class schedule app for home schooling & remote education.